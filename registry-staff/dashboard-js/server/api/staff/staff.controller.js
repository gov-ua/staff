'use strict';

var activiti = require('../../components/activiti');
//var logger = require('../../components/logger').setup();
//logger.info('Express server listening on %d, in %s mode', config.port, app.get('env'));

exports.register1 = function (req, res) {
  var query = {};
  var options = {
    path: 'staff/register1',
    contentType : 'application/json',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
  };
  activiti.get(options, function (error, statusCode, result) {
    if (error) {
      res.send(error);
    } else {
      res.json(result);
    }
  });
};

exports.register = function (req, res) {
  //var userReg = JSON.parse(req.userReg);

  var query = {
    //sFirstName : 'Test',
    //json : 'true',
    //sLastName  : req.query.sLastName  ,
    //sMiddleName: req.query.sMiddleName,
    //sLogin     : req.query.sLogin     ,
    //sPassword  : req.query.sPassword  ,
    //sPhone     : req.query.sPhone     ,
    //sEmail     : req.query.sEmail     ,
    //oPosition  : req.query.oPosition  ,
    //oOrgan     : req.query.oOrgan
  };
  var options = {
    path: 'staff/register',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}//,
    //query: query
  };
  activiti.post(options, function(error, statusCode, result) {
    res.statusCode = statusCode;
    res.send(result);
  }, req.body);
};
exports.getOrgans = function (req, res) {
  var query = {};
  var options = {
    path: 'staff/getOrgans',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
  };
  activiti.get(options, function (error, statusCode, result) {
    if (error) {
      res.send(error);
    } else {
      res.json(result);
    }
  });
};
exports.getPositions = function (req, res) {
  var query = {};
  var options = {
    path: 'staff/getPositions',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
  };
  activiti.get(options, function (error, statusCode, result) {
    if (error) {
      res.send(error);
    } else {
      res.json(result);
    }
  });
};

exports.getOrganByUser = function (req, res) {
  var options = {
    path: 'staff/getOrganByUser',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='},
    query: {
      userId:  req.query.userId
    }
  };
  activiti.get(options, function (error, statusCode, result) {
    if (error) {
      res.send(error);
    } else {
      res.status(200).send(result);
    }
  });
};
exports.getGroups = function (req, res) {
  var query = {};
  var options = {
    path: 'staff/getGroups',
    headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
  };
  activiti.get(options, function (error, statusCode, result) {
    if (error) {
      res.send(error);
    } else {
      res.json(result);
    }
  });
};
exports.generateReportInXls = function (req, res) {
  var options = {

    path: 'staff/generateReportInXls',
    query: {
      'organName' : req.query.organName,
      'yearFrom'  : req.query.yearFrom,
      'yearTo'    : req.query.yearTo,
      'quaterFrom': req.query.quaterFrom,
      'quaterTo'  : req.query.quaterTo
    }
  };
  activiti.filedownload(req, res, options);
};
