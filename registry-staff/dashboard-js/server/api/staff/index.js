'use strict';

var express = require('express');
var controller = require('./staff.controller');

var router = express.Router();

router.post('/register'           , controller.register    );
router.get ('/register'           , controller.register    );
router.get ('/getOrgans'          , controller.getOrgans   );
router.get ('/getPositions'       , controller.getPositions);
router.get ('/getGroups'          , controller.getGroups   );
router.get ('/getOrganByUser'     , controller.getOrganByUser);
router.get ('/generateReportInXls', controller.generateReportInXls);

module.exports = router;
