'use strict';

angular.module('dashboardJsApp')
  .controller('ReportsCtrl', function ($scope, $timeout, Modal, Auth, Reg, reports, processes) {
    $scope.export = {};
    $scope.export.from = '2015-06-01';
    $scope.export.to = '2015-08-01';
    $scope.export.sBP = 'dnepr_spravka_o_doxodax';
    $scope.exportURL = "/reports";

    $scope.organ={}
    $scope.statistic = {};
    $scope.statistic.quaterFrom = '4';
    $scope.statistic.yearFrom   = '2015';
    $scope.statistic.quaterTo   = '4';
    $scope.statistic.yearTo     = '2015';
    $scope.statistic.sBP = 'dnepr_spravka_o_doxodax';
    $scope.statisticUrl = "/generateReportInXls";

    var user = Auth.getCurrentUser();
    Reg.getOrganByUser($scope, user.id);

     $scope.initExportUrl = function () {
        reports.exportLink({ from: $scope.export.from, to: $scope.export.to, sBP: $scope.export.sBP},
            function (result) {
                $scope.exportURL = result;
            });
    }

     $scope.getExportLink = function () {
        //$scope.initExportUrl();
          return $scope.exportURL;
      }

      $scope.initStatisticUrl = function () {
        reports.statisticLink({ quaterFrom: $scope.statistic.quaterFrom, yearFrom: $scope.statistic.yearFrom,
                                quaterTo :  $scope.statistic.quaterTo  , yearTo  : $scope.statistic.yearTo,
                                organName:  $scope.organ.sName},
            function (result) {
                $scope.statisticUrl = result;
            });
    }

    $scope.getStatisticLink = function () {
          $scope.initStatisticUrl();
          return $scope.statisticUrl;
    }

    processes.getUserProcesses().then(function (data) {
      $scope.processesList = data;
      if ($scope.processesList != null && $scope.processesList != '' && $scope.processesList.length > 0) {
        $scope.statistic.sBP = $scope.processesList[0].sID;
        $scope.export.sBP = $scope.processesList[0].sID;
        $scope.initExportUrl();
        $scope.initStatisticUrl();
      }
    }, function () {
      $scope.processesList = "error";
    });

    $scope.processesLoaded = function() {
      if ($scope.processesList)
      return true;
    return false;
    }

     $scope.processesLoadError = function() {
      if ($scope.processesList && $scope.processesList == "error")
      return true;
    return false;
    }

  });
