'use strict';
angular.module('dashboardJsApp').factory('simpleFieldService', function () {

  var fieldsRules = {
    long: {
      '*': {
        pattern: '[0-9]+',
        message: 'Некоректне значення. Поле може складатися з цілих чисел.'
      }
    },
    string: {
      'email': {
        pattern: '[A-za-z0-9]+[A-za-z0-9._-]+@[a-z]+\.[a-z.-]{2,20}',
        message: 'Некоректний Email.'
      },
      'phone': {
        pattern: '\\+38([0-9]{10,13})',
        message: 'Невiрно вказаний номер (формат +380...)'
      },
      'index': {
        pattern: '^\\d{5}$',
        message: 'Некоректний індекс.'
      }
    }
  };

  var findRuleForItem = function (item) {
    if (angular.isDefined(fieldsRules[item.type])) {
      for (var key in fieldsRules[item.type]) {
        if (fieldsRules[item.type].hasOwnProperty(key)) {
          if (key == '*' || item.id == key || item.id.match(new RegExp('\\.' + key + '$')))
            return fieldsRules[item.type][key];
        }
      }
    }
  };

  return {
    getFieldPattern: function (item) {
      var rule = findRuleForItem(item);
      if (rule)
        return rule.pattern;
    },
    getFieldErrorMessage: function (item) {
      var rule = findRuleForItem(item);
      if (rule)
        return rule.message;
    }
  }
});
