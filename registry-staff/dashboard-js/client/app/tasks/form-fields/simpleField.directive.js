'use strict';
angular.module('dashboardJsApp').directive('simpleField', function(simpleFieldService) {
  return {

    restrict: 'E',

    templateUrl: 'app/tasks/form-fields/simpleField.html',

    scope: {
      item: '=',
      ngDisabled: '=',
      submitted: '='
    },

    link: function(scope) {
      scope.getPattern = function () {
        return simpleFieldService.getFieldPattern(scope.item);
      };

      scope.getErrorMessage = function () {
        return simpleFieldService.getFieldErrorMessage(scope.item);
      }
    }

  };
});
