'use strict';

angular.module('dashboardJsApp')
  .factory('Reg', function($location, $rootScope, $http, User, Base64, $cookieStore, $q) {
    /**currentUser: Object
     email: "kermit@activiti.org"
     firstName: "Kermit"
     id: "kermit"
     lastName: "The Frog"
     pictureUrl: "https://52.17.126.64:8080/wf/service/identity/users/kermit/picture"
     url: "https://52.17.126.64:8080/wf/service/identity/users/kermit"
     **/
    var currentUser = {};
    //var sessionSettings;


    //if ($cookieStore.get('sessionSettings')) {
    //  sessionSettings = $cookieStore.get('sessionSettings');
    //}

    return {
      /**
       * Authenticate user and save user data
       *
       * @param  {Object}   user     - login info
       * @param  {Function} callback - optional
       * @return {Promise}
       */
      register: function($scope) {
        //var cb = callback || angular.noop;
        var deferred = $q.defer();

        var req = {
          method: 'POST',
          url: '/staff/register',
          headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='},
          data:  { sFirstName : $scope.userReg.sFirstName ,
                   sLastName  : $scope.userReg.sLastName  ,
                   sMiddleName: $scope.userReg.sMiddleName,
                   sLogin     : $scope.userReg.sLogin     ,
                   sPassword  : $scope.userReg.sPassword  ,
                   sPhone     : $scope.userReg.sPhone     ,
                   sEmail     : $scope.userReg.sEmail     ,
                   sPosition  : $scope.userReg.sPosition  ,
                   sOrgan     : $scope.userReg.sOrgan     }
        };


        //$http(req).
        //success(function(data) {
        //  //currentUser = JSON.parse(data);
        //  deferred.resolve(data);
        //  //return cb();
        //}).
        //error(function(err) {
        //
        //  deferred.reject(err);
        //  //return cb(err);
        //}.bind(this));
        //

        $http(req).
          success(function(data) {
            //currentUser = JSON.parse(data);
            deferred.resolve(data);
            //return cb();
          })
          .then(function() {
            // Logged in, redirect to home
            $location.path('/main');
            alert('Ви успішно зареєстровані на порталі!');
          })
          .catch(function(err) {
            $scope.errors = err.data.message;
            console.log(err);
          });

        return deferred.promise;
      },
      getOrgans: function($scope){
        return $http({
          method: 'GET',
          url: '/staff/getOrgans',
          //headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
        }).success(function (result) {
          $scope.organs = JSON.parse(result);
        })
      },
      getPositions: function($scope){
        return $http({
          method: 'GET',
          url: '/staff/getPositions',
          //headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
        }).success(function (result) {
          $scope.positions = JSON.parse(result);
        });
      },
      getGroups: function($scope){
        return $http({
          method: 'GET',
          url: '/staff/getGroups',
          //headers: {'Authorization': 'Basic YWN0aXZpdGktbWFzdGVyOlVqaHRKbkV2ZiE='}
        }).success(function (result) {
          $scope.groups = JSON.parse(result);
        });
      },
      getOrganByUser: function($scope, login) {
        $http({
          method: 'GET',
          url: '/staff/getOrganByUser',
          params: {
            userId: login
          }
        }).success(function (result) {
          $scope.organ = JSON.parse(JSON.stringify(result));
        });
      }
    };
  });
