'use strict';

angular.module('dashboardJsApp')
	.config(function($routeProvider) {
    $routeProvider
      .when('/instruction',{
        templateUrl: 'app/instruction/instruction.html'
      })
      .when('/register',{
        templateUrl: 'app/register/register.html',
        controller: 'RegisterCtrl'
      })
			.when('/', {
				templateUrl: 'app/main/main.html',
				controller: 'MainCtrl'
			});
	}
);
