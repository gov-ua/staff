package org.igov.staff.dao;

import org.igov.staff.model.OrganTypeDetailed;
import org.igov.model.core.EntityDao;

public interface OrganTypeDetailedDao extends EntityDao<Long, OrganTypeDetailed> {

}

