package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.igov.staff.model.Period;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class PeriodDaoImpl extends GenericEntityDao<Long, Period> implements PeriodDao {

    private static final Logger log = Logger.getLogger(PeriodDaoImpl.class);

    protected PeriodDaoImpl() {
        super(Period.class);
    }
    
    @Override
    public Period getPeriod(Integer nYear, Integer nQuater){
        Criteria criteria = getSession().createCriteria(Period.class);
        criteria.add(Restrictions.eq("nYear", nYear));
        criteria.add(Restrictions.eq("nQuater", nQuater));
        Period period = (Period) criteria.uniqueResult();
        return period;
}
}
