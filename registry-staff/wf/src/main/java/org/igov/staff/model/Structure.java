package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Structure extends NamedEntity implements  WfClonable<Structure>{
    @Transient
    @Override
    public Structure clone() throws CloneNotSupportedException {
        return (Structure)super.clone();
    }

}
