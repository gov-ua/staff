package org.igov.staff.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.*;
import org.igov.model.core.AbstractEntity;

/**
 * Created by Sergey_PC on 24.10.2015.
 */

@javax.persistence.Entity
public class SubjectReport_StructureOrgan extends AbstractEntity 
                  implements WfClonable<SubjectReport_StructureOrgan>, Serializable {

    @JsonProperty(value = "oStructureOrgan")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "nID_StructureOrgan", nullable = true)
    private StructureOrgan oStructureOrgan;

    @JsonProperty(value = "oPeriod")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Period")
    private Period oPeriod;

    @JsonProperty(value = "aSubjectReport_Position")
    @OneToMany(mappedBy = "oSubjectReport_StructureOrgan", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<SubjectReport_Position> aSubjectReport_Position = new ArrayList<>();

    @JsonProperty(value = "nCountOrgan")
    @Column
    private Integer nCountOrgan;
    
    @JsonProperty(value = "nCountPlanOther")
    @Column
    private Integer nCountPlanOther;
    
    @JsonProperty(value = "nCountVacancyOther")
    @Column
    private Integer nCountVacancyOther;
    
    @JsonProperty(value = "nCountAccount")
    @Column
    private Integer nCountAccount;

    public SubjectReport_StructureOrgan() {}

    public StructureOrgan getoStructureOrgan() {
        return oStructureOrgan;
    }

    public void setoStructureOrgan(StructureOrgan oStructureOrgan) {
        this.oStructureOrgan = oStructureOrgan;
    }

    public Period getoPeriod() {
        return oPeriod;
    }

    public void setoPeriod(Period oPeriod) {
        this.oPeriod = oPeriod;
    }

    public List<SubjectReport_Position> getaSubjectReport_Position() {
        return aSubjectReport_Position;
    }

    public void setaSubjectReport_Position(
            List<SubjectReport_Position> aSubjectReport_Position) {
        this.aSubjectReport_Position = aSubjectReport_Position;
    }

    public Integer getnCountOrgan() {
        return nCountOrgan;
    }

    public void setnCountOrgan(Integer nCountOrgan) {
        this.nCountOrgan = nCountOrgan;
    }

    public Integer getnCountPlanOther() {
        return nCountPlanOther;
    }

    public void setnCountPlanOther(Integer nCountPlanOther) {
        this.nCountPlanOther = nCountPlanOther;
    }

    public Integer getnCountVacancyOther() {
        return nCountVacancyOther;
    }

    public void setnCountVacancyOther(Integer nCountVacancyOther) {
        this.nCountVacancyOther = nCountVacancyOther;
    }

    public Integer getnCountAccount() {
        return nCountAccount;
    }

    public void setnCountAccount(Integer nCountAccount) {
        this.nCountAccount = nCountAccount;
    }
    
    @Transient
    @Override
    public SubjectReport_StructureOrgan clone() throws CloneNotSupportedException {
        return (SubjectReport_StructureOrgan) super.clone();
    }

    @Transient
    public void addReportPosition(List<SubjectReport_Position> list){
        aSubjectReport_Position.addAll(list);
    }

    @JsonIgnore
    @Transient
    public SubjectReport_StructureOrgan getCopyWithoutStructurePositions() throws CloneNotSupportedException {
        SubjectReport_StructureOrgan o = this.clone();
        o.setaSubjectReport_Position(null);
        return o;
    }

}
