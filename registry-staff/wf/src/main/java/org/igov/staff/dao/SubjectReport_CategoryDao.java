package org.igov.staff.dao;

import java.util.List;
import org.igov.staff.model.SubjectReport_Category;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectReport_CategoryDao extends EntityDao<Long, SubjectReport_Category> {
    
    List<SubjectReport_Category> getSubjectReport_CategoriesByYearAndQuater(Integer nYearFrom, Integer nYearTo, 
            Integer nQuaterFrom, Integer nQuaterTo, String sOrganName);
    
}
