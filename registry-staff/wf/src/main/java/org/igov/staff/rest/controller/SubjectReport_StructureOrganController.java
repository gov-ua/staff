package org.igov.staff.rest.controller;

import com.google.common.base.Optional;
import org.igov.staff.dao.SubjectReport_StructureOrganDao;
import org.igov.staff.model.SubjectReport_StructureOrgan;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author dmaidaniuk
 */
@Controller
@RequestMapping(value = "/staff", produces = {APPLICATION_JSON_VALUE})
public class SubjectReport_StructureOrganController {
    
    @Autowired
    private SubjectReport_StructureOrganDao dao;
    
    @RequestMapping(value = "/getSubjectReportStructureOrgan", method = GET, produces = {APPLICATION_JSON_VALUE})
    public @ResponseBody
    SubjectReport_StructureOrgan findById(@RequestParam(value = "id", required = true) Long id) {
        Optional<SubjectReport_StructureOrgan> entity = dao.findById(id);
        if (entity.isPresent()) {
            return entity.get();
        }
        return null;
    }
    
}
