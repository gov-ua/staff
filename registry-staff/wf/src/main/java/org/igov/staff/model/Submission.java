package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Submission extends NamedEntity implements WfClonable<Submission>{
    @Transient
    @Override
    public Submission clone() throws CloneNotSupportedException {
        return (Submission)super.clone();
    }
}
