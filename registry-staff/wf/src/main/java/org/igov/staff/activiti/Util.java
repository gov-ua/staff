/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.igov.staff.activiti;

/**
 *
 * @author olya
 */
public class Util {

    public static boolean isItAdmin(String nID_Group) {
        return nID_Group != null && (nID_Group.contains("admin") || "sales".equalsIgnoreCase(nID_Group));
    }
}
