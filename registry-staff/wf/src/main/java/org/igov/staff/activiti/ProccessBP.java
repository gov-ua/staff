package org.igov.staff.activiti;

public enum ProccessBP {
    formTitle(true),
    formCountStaff(false),
    formCountHired(false),
    formCountFired(false),
    countStaff(true),
    countHired(true);
    
    private final boolean isActual;
    
    private ProccessBP(boolean isActual){
        this.isActual = isActual;
    }
    
    public boolean isActual(){
        return isActual;
    }
}

