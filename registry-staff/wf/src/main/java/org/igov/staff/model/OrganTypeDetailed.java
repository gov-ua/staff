package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class OrganTypeDetailed extends NamedEntity implements WfClonable<OrganTypeDetailed>{
    @Transient
    @Override
    public OrganTypeDetailed clone() throws CloneNotSupportedException {
        return (OrganTypeDetailed)super.clone();
    }
}
