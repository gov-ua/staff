package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class OrganTypeCommon extends NamedEntity implements WfClonable<OrganTypeCommon>{
    @Transient
    @Override
    public OrganTypeCommon clone() throws CloneNotSupportedException {
        return (OrganTypeCommon)super.clone();
    }
}
