package org.igov.staff.dao;

import org.igov.staff.model.SubjectReport_Position;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectReport_PositionDao extends EntityDao<Long, SubjectReport_Position> {
}
