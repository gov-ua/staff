package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Position;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class PositionDaoImpl extends GenericEntityDao<Long, Position> implements PositionDao  {

    private static final Logger log = Logger.getLogger(PositionDaoImpl.class);

    protected PositionDaoImpl() {
        super(Position.class);
    }
}
