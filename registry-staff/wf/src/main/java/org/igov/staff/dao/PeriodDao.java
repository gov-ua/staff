package org.igov.staff.dao;

import org.igov.staff.model.Period;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface PeriodDao extends EntityDao<Long, Period> {
    
    public Period getPeriod(Integer nYear, Integer nQuater);
}
