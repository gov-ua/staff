/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.igov.staff.activiti.logic;

import org.igov.staff.activiti.logic.Identity.Role;

/**
 *
 * @author olya
 */
public interface IdentityInterface {
    
    public boolean hasRights(String organName, Role role);
    
}
