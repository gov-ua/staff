package org.igov.staff.activiti.serviceTask;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.igov.staff.core.StringUtil;
import org.igov.staff.dao.OrganDao;
import org.igov.staff.model.Organ;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.igov.staff.activiti.Constant;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

@Component("saveFormTitle")
public class SaveFormTitle implements JavaDelegate {

    private final static Logger LOG = LoggerFactory.getLogger(SaveFormTitle.class);

    @Autowired
    private OrganDao organDao;

    @Override
    public void execute(final DelegateExecution execution) throws Exception {
        String groupName;
        Organ  organ;
        StringBuilder sbAddr = new StringBuilder();
        Map<String, Object> localVariable = execution.getVariablesLocal();
        LOG.info("localVariable: " + localVariable);
        if (localVariable != null) {
            groupName = (String) execution.getVariable(Constant.organ_ID);
            organ = organDao.getOrgan(groupName);
            if(organ!= null){
                sbAddr.append(StringUtil.trimStrAndSetDivIfNeed((String) localVariable.get(Constant.sRegion  ), " ", ","));
                sbAddr.append(StringUtil.trimStrAndSetDivIfNeed((String) localVariable.get(Constant.sDistrict), " ", ","));
                sbAddr.append(StringUtil.trimStrAndSetDivIfNeed((String) localVariable.get(Constant.sCity    ), " ", ","));
                sbAddr.append(StringUtil.trimStrAndSetDivIfNeed((String) localVariable.get(Constant.sStreet  ), " ", ","));
                sbAddr.append(StringUtil.trimStrAndSetDivIfNeed((String) localVariable.get(Constant.sBuild   ), " ", "" ));
                if(sbAddr.length()>0)
                    organ.setsAddress(sbAddr.toString());
                organ.setsIndex((String) localVariable.get(Constant.sIndex));
                organ.setsPhone((String) localVariable.get(Constant.sPhone));
                organ.setsFax  ((String) localVariable.get(Constant.sFax  ));
                organ.setsEmail((String) localVariable.get(Constant.sEmail));

                organ = organDao.saveOrUpdate(organ);
                System.out.println(organ);
            }
        }
    }


}
