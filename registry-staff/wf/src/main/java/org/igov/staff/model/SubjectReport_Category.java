package org.igov.staff.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import org.igov.model.core.AbstractEntity;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class SubjectReport_Category extends AbstractEntity implements WfClonable<SubjectReport_Category>, Serializable {

    @JsonProperty(value = "oCategory")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Category", nullable = false)
    private Categori oCategory;

    @JsonIgnore
//    @JsonProperty(value = "aSubjectReport_Position")
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = SubjectReport_Position.class, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "nID_SubjectReport_Position")
    private SubjectReport_Position oSubjectReport_Position;

    @JsonProperty(value = "nCountHired")
    @Column
    private Integer nCountHired;

    @JsonProperty(value = "nCountHiredCompet")
    @Column
    private Integer nCountHiredCompet;

    @JsonProperty(value = "nCountFired")
    @Column
    private Integer nCountFired;

    @JsonProperty(value = "nCountPlan")
    @Column
    private Integer nCountPlan;

    @JsonProperty(value = "nCountFact")
    @Column
    private Integer nCountFact;

    @JsonProperty(value = "nCountVacancy")
    @Column
    private Integer nCountVacancy;
    
    @JsonProperty(value = "nCountReal")
    @Column
    private Integer nCountReal;
    
    @JsonProperty(value = "nCountTransfer")
    @Column
    private Integer nCountTransfer;

    public Integer getnCountHired() {
        return nCountHired;
    }

    public void setnCountHired(Integer nCountHired) {
        this.nCountHired = nCountHired;
    }

    public Integer getnCountHiredCompet() {
        return nCountHiredCompet;
    }

    public void setnCountHiredCompet(Integer nCountHiredCompet) {
        this.nCountHiredCompet = nCountHiredCompet;
    }

    public Integer getnCountFired() {
        return nCountFired;
    }

    public void setnCountFired(Integer nCountFired) {
        this.nCountFired = nCountFired;
    }

    public Integer getnCountPlan() {
        return nCountPlan;
    }

    public void setnCountPlan(Integer nCountPlan) {
        this.nCountPlan = nCountPlan;
    }

    public Integer getnCountFact() {
        return nCountFact;
    }

    public void setnCountFact(Integer nCountFact) {
        this.nCountFact = nCountFact;
    }

    public Integer getnCountVacancy() {
        return nCountVacancy;
    }

    public void setnCountVacancy(Integer nCountVacancy) {
        this.nCountVacancy = nCountVacancy;
    }

    public Categori getoCategory() {
        return oCategory;
    }

    public void setoCategory(Categori oCategory) {
        this.oCategory = oCategory;
    }

    public SubjectReport_Position getoSubjectReport_Position() {
        return oSubjectReport_Position;
    }

    public void setoSubjectReport_Position(
            SubjectReport_Position oSubjectReport_Position) {
        this.oSubjectReport_Position = oSubjectReport_Position;
    }

    public Integer getnCountReal() {
        return nCountReal;
    }

    public void setnCountReal(Integer nCountReal) {
        this.nCountReal = nCountReal;
    }

    public Integer getnCountTransfer() {
        return nCountTransfer;
    }

    public void setnCountTransfer(Integer nCountTransfer) {
        this.nCountTransfer = nCountTransfer;
    }
    
    @Transient
    @Override
    public SubjectReport_Category clone() throws CloneNotSupportedException {
        return (SubjectReport_Category) super.clone();
    }

}
