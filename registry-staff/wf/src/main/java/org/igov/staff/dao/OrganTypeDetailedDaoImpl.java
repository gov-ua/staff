package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.OrganTypeDetailed;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
@Repository
public class OrganTypeDetailedDaoImpl extends GenericEntityDao<Long, OrganTypeDetailed> implements OrganTypeDetailedDao {

    private static final Logger log = Logger.getLogger(OrganTypeDetailedDaoImpl.class);

    protected OrganTypeDetailedDaoImpl() {
        super(OrganTypeDetailed.class);
    }

}
