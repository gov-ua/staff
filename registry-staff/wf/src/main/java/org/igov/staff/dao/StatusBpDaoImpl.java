package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.StatusBP;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class StatusBpDaoImpl extends GenericEntityDao<Long, StatusBP> implements StatusBpDao {

    private static final Logger log = Logger.getLogger(PositionDaoImpl.class);

    protected StatusBpDaoImpl() {
        super(StatusBP.class);
    }
}
