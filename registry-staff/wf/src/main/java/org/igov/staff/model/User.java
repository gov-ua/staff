package org.igov.staff.model;

import javax.persistence.Column;
import javax.persistence.Transient;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.igov.model.core.AbstractEntity;

@javax.persistence.Entity
public class User extends AbstractEntity implements WfClonable<User> {

    @JsonProperty(value = "sFirstName", required = true)
    @Column
    private String sFirstName;

    @JsonProperty(value = "sLastName", required = true)
    @Column
    private String sLastName;

    @JsonProperty(value = "sMiddleName", required = true)
    @Column
    private String sMiddleName;

    @JsonProperty(value = "sLogin", required = true)
    @Column
    private String sLogin;

    @Transient
    @JsonProperty(value = "sPassword", required = true)
    @Column
    private String sPassword;

    @JsonProperty(value = "sPhone", required = true)
    @Column
    private String sPhone;

    @JsonProperty(value = "sEmail", required = true)
    @Column
    private String sEmail;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Position", nullable = true)
    private Position oPosition;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Organ", nullable = false)
    private Organ oOrgan;

    @JsonProperty(value = "sPosition", required = true)
    private String sPosition;

    @Transient
    @JsonProperty(value = "sOrgan", required = true)
    private String sOrgan;


    public String getsLastName() {
        return sLastName;
    }

    public void setsLastName(String sLastName) {
        this.sLastName = sLastName;
    }

    public String getsFirstName() {
        return sFirstName;
    }

    public void setsFirstName(String sFirstName) {
        this.sFirstName = sFirstName;
    }

    public String getsMiddleName() {
        return sMiddleName;
    }

    public void setsMiddleName(String sMiddleName) {
        this.sMiddleName = sMiddleName;
    }

    public String getsLogin() {
        return sLogin;
    }

    public void setsLogin(String sLogin) {
        this.sLogin = sLogin;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String sPhone) {
        this.sPhone = sPhone;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public Position getoPosition() {
        return oPosition;
    }

    public void setoPosition(Position oPosition) {
        this.oPosition = oPosition;
    }

    public Organ getoOrgan() {
        return oOrgan;
    }

    public void setoOrgan(Organ oOrgan) {
        this.oOrgan = oOrgan;
    }

    public String getsPosition() {
        return sPosition;
    }

    public void setsPosition(String sPosition) {
        this.sPosition = sPosition;
    }

    public String getsOrgan() {
        return sOrgan;
    }

    public void setsOrgan(String sOrgan) {
        this.sOrgan = sOrgan;
    }

    @Transient
    @Override
    public User clone() throws CloneNotSupportedException {
        return (User) super.clone();
    }

    public String getsPassword() {
        return sPassword;
    }

    public void setsPassword(String sPassword) {
        this.sPassword = sPassword;
    }

}
