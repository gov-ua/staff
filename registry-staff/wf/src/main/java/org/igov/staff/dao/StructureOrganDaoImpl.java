package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.igov.staff.model.StructureOrgan;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;
import java.util.List;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class StructureOrganDaoImpl  extends GenericEntityDao<Long, StructureOrgan>  implements StructureOrganDao{

    private static final Logger log = Logger.getLogger(StructureOrgan.class);

    protected StructureOrganDaoImpl() {
        super(StructureOrgan.class);
    }

    @Override
    public List<StructureOrgan> getaStructureOrganByID_Organ(Long id) {
        Criteria criteria = getSession().createCriteria(StructureOrgan.class);
        criteria.add(Restrictions.eq("oOrgan.id", id));
        List<StructureOrgan> aStructureOrgan = (List<StructureOrgan>) criteria.list();
        return aStructureOrgan;
    }
}
