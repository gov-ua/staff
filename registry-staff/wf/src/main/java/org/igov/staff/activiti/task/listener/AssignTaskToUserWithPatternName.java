package org.igov.staff.activiti.task.listener;

import org.activiti.engine.IdentityService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.identity.User;
import org.activiti.engine.impl.el.FixedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("assignTaskToUserWithPatternName")
public class AssignTaskToUserWithPatternName implements TaskListener{

    @Autowired
    private IdentityService identityService;
    
    private FixedValue patternName = null;
    
    @Override
    public void notify(DelegateTask delegateTask) {
        DelegateExecution execution = delegateTask.getExecution(); 
        User user = identityService.createUserQuery()
                .userId((String)execution.getVariable("act_id_group.id_") + patternName)
                .singleResult(); 
        delegateTask.setAssignee(user.getId());
    }

}
