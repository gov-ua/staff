package org.igov.staff.dao;

import org.igov.staff.model.Structure;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface StructureDao extends EntityDao<Long, Structure> {
}
