package org.igov.staff.dao;

import org.igov.staff.model.LanchBP;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface LanchBpDao extends EntityDao<Long, LanchBP> {
}
