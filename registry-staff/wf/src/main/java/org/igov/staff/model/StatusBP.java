package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class StatusBP extends NamedEntity implements WfClonable<StatusBP>{
    @Transient
    @Override
    public StatusBP clone() throws CloneNotSupportedException {
        return (StatusBP)super.clone();
    }
}
