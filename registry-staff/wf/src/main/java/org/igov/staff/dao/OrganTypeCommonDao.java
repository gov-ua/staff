package org.igov.staff.dao;

import org.igov.staff.model.OrganTypeCommon;
import org.igov.model.core.EntityDao;

public interface OrganTypeCommonDao extends EntityDao<Long, OrganTypeCommon> {

}

