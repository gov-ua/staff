package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Submission;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
@Repository
public class SubmissionDaoImpl extends GenericEntityDao<Long, Submission> implements SubmissionDao {

    private static final Logger log = Logger.getLogger(SubmissionDaoImpl.class);

    protected SubmissionDaoImpl() {
        super(Submission.class);
    }

}
