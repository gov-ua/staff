package org.igov.staff.activiti.serviceTask;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.igov.staff.activiti.Constant;
import org.igov.staff.dao.SubjectReport_StructureOrganDao;
import org.igov.staff.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Map;

@Component("saveFormCountStaff")
public class SaveFormCountStaff implements JavaDelegate {

    private final static Logger LOG = LoggerFactory.getLogger(SaveFormCountStaff.class);

    @Autowired
    private SubjectReport_StructureOrganDao reportStructureOrganDao;

    @Override
    public void execute(final DelegateExecution execution) throws Exception {
        Long nID_SubjectReport_StructureOrgan;

        SubjectReport_StructureOrgan subjectReport_structureOrgan;
        Map<String, Object> localVariable = execution.getVariablesLocal();
        FormForSave formForSave;
        LOG.info("localVariable: " + localVariable);
        if (localVariable != null) {
            nID_SubjectReport_StructureOrgan = (Long) execution.getVariable(Constant.nID_SubjectReport_SructureOrgan);
            LOG.info("nID_SubjectReport_StructureOrgan: " + nID_SubjectReport_StructureOrgan);
            subjectReport_structureOrgan = reportStructureOrganDao.findByIdExpected(nID_SubjectReport_StructureOrgan);
            if (execution.getVariableLocal(Constant.nCountOrgan) != null) {
                subjectReport_structureOrgan.setnCountOrgan(((Long) execution.getVariableLocal(Constant.nCountOrgan)).intValue());
            }
            if (execution.getVariableLocal(Constant.nCountPlanOther) != null) {
                subjectReport_structureOrgan.setnCountPlanOther(((Long) execution.getVariableLocal(Constant.nCountPlanOther)).intValue());
            }
            if (execution.getVariableLocal(Constant.nCountVacancyOther) != null) {
                subjectReport_structureOrgan.setnCountVacancyOther(((Long) execution.getVariableLocal(Constant.nCountVacancyOther)).intValue());
            }
            if (execution.getVariableLocal(Constant.nCountAccount) != null) {
                subjectReport_structureOrgan.setnCountAccount(((Long) execution.getVariableLocal(Constant.nCountAccount)).intValue());
            }
            // System.out.println("Retur!!! - " + subjectReport_structureOrgan != null);
            if (subjectReport_structureOrgan != null) {
                for (Map.Entry<String, Object> entry : localVariable.entrySet()) {
                    if ((formForSave = parseKeyFromStr(entry.getKey())) != null) {
                        if (formForSave.getField() != null && entry.getValue() != null) {
                            searchAndSetValueToEndPoint(subjectReport_structureOrgan, formForSave.getPosition(),
                                    formForSave.getCategory(), formForSave.getField(), ((Long) entry.getValue()).intValue());
                        }
                    }
                }
                LOG.info("reportStructureOrganDao...");
                subjectReport_structureOrgan = reportStructureOrganDao.saveOrUpdate(subjectReport_structureOrgan);
                LOG.info("reportStructureOrganDao ок!!!!!!!!!!!!");
            }
        }
    }

    protected FormForSave parseKeyFromStr(String str) {
        FormForSave form = new FormForSave();
        String[] groups;
        String[] keyVal;
        if (str != null && !str.isEmpty()) {
            if (str.startsWith("nCount")) {
                groups = str.split(";");
                if (groups != null && groups.length > 0) {
                    for (String g : groups) {
                        if (g != null && !g.isEmpty()) {
                            if (g.startsWith("nCount")) {
                                form.setField(g);
                            } else if ((keyVal = g.split(":")) != null && keyVal.length == 2) {
                                if (keyVal[0].equalsIgnoreCase(Constant.nID_Positiont)) {
                                    form.setPosition(keyVal[1] != null ? Long.valueOf(keyVal[1]) : 0);
                                } else if (keyVal[0].equalsIgnoreCase(Constant.nID_Category)) {
                                    form.setCategory(keyVal[1] != null ? Long.valueOf(keyVal[1]) : 0);
                                }
                            }
                        }
                    }
                }
            }
        }
        LOG.info("form: " + form.getField() + ":" + form.getPosition() + ":" + form.getCategory());
        return form;
    }

    protected void searchAndSetValueToEndPoint(SubjectReport_StructureOrgan subjectReport_structureOrgan,
            Long positionId, Long categoryId, String fieldName, Integer value) {
        SubjectReport_Position resultReportPosition = null;
        List<SubjectReport_Position> positions;
        List<SubjectReport_Category> categories;
        if (subjectReport_structureOrgan != null) {
            if (positionId != null) {
                if ((positions = subjectReport_structureOrgan.getaSubjectReport_Position()) != null) {
                    Position position;
                    for (SubjectReport_Position subjectReport_Position : positions) {
                        if ((position = subjectReport_Position.getoPosition()) != null && position.getId().equals(positionId)) {
                            resultReportPosition = subjectReport_Position;
                            if (fieldName != null && !fieldName.isEmpty()) {
                                switch (fieldName) {
                                    case "nCountHired_Re":
                                        subjectReport_Position.setnCountHired_Re(value);
                                        break;
                                    case "nCountHiredCompet_Re":
                                        subjectReport_Position.setnCountHiredCompet_Re(value);
                                        break;
                                }
                            }
                        }
                    }
                }
                if (categoryId != null && resultReportPosition != null) {
                    if ((categories = resultReportPosition.getaSubjectReport_Category()) != null) {
                        Categori category;
                        for (SubjectReport_Category report_Category : categories) {
                            if ((category = report_Category.getoCategory()) != null && category.getId().equals(categoryId)) {
                                if (fieldName != null && !fieldName.isEmpty()) {
                                    switch (fieldName) {
                                        case "nCountHired":
                                            report_Category.setnCountHired(value);
                                            break;
                                        case "nCountHiredCompet":
                                            report_Category.setnCountHiredCompet(value);
                                            break;
                                        case "nCountFired":
                                            report_Category.setnCountFired(value);
                                            break;
                                        case "nCountPlan":
                                            report_Category.setnCountPlan(value);
                                            break;
                                        case "nCountFact":
                                            report_Category.setnCountFact(value);
                                            break;
                                        case "nCountVacancy":
                                            report_Category.setnCountVacancy(value);
                                            break;
                                        case "nCountReal":
                                            report_Category.setnCountReal(value);
                                            break;
                                        case "nCountTransfer":
                                            report_Category.setnCountTransfer(value);
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
