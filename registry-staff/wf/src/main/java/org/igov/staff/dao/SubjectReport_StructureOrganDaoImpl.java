package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.SubjectReport_StructureOrgan;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class SubjectReport_StructureOrganDaoImpl extends GenericEntityDao<Long, SubjectReport_StructureOrgan> 
                                                            implements SubjectReport_StructureOrganDao {

    private static final Logger log = Logger.getLogger(SubjectReport_StructureOrganDaoImpl.class);

    protected SubjectReport_StructureOrganDaoImpl() {
        super(SubjectReport_StructureOrgan.class);
    }

}
