package org.igov.staff.rest.controller;

import org.igov.staff.dao.PositionDao;
import org.igov.staff.model.Position;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;

/**
 * Created by Sergey_PC on 28.11.2015.
 */
@Controller
@RequestMapping(value = "/staff", produces = {MediaType.APPLICATION_JSON_VALUE})
public class PositionController {

    @Autowired
    private PositionDao positionDao;

    @RequestMapping(value = "/getPositions", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Position> getPositions() {
        return positionDao.findAll();
    }
}
