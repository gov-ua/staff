package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.SubjectReport_Position;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class SubjectReport_PositionDaoImpl extends GenericEntityDao<Long, SubjectReport_Position> implements SubjectReport_PositionDao {

    private static final Logger log = Logger.getLogger(PositionDaoImpl.class);

    protected SubjectReport_PositionDaoImpl() {
        super(SubjectReport_Position.class);
    }
}
