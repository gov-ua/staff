package org.igov.staff.activiti.task.listener;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.springframework.stereotype.Component;

@Component("assignTaskToGroup")
public class AssignTaskToGroup implements TaskListener{

    @Override
    public void notify(DelegateTask delegateTask) {
        DelegateExecution execution = delegateTask.getExecution();
        delegateTask.addCandidateGroup((String)execution.getVariable("act_id_group.id_"));
    }

}
