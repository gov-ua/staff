package org.igov.staff.activiti.logic;

import java.util.List;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Identity implements IdentityInterface{
    
    private static final Logger LOG = LoggerFactory.getLogger(Identity.class);

    @Autowired
    private IdentityService identityService;

    private static final String ROLE_REQUIRED_ERROR = "Role parameter should be present.";

    public enum Role {

        ASSIGNMENT("assignment"),
        SECURITY("security-role");

        private final String nID_Role;

        private Role(String nID_Role) {
            this.nID_Role = nID_Role;
        }

    }

    @Override
    public boolean hasRights(String organName, Role role) {
        if (role == null) {
            throw new IllegalStateException(ROLE_REQUIRED_ERROR);
        }
        
        boolean allowed = false;
        if (organName != null) {
            Group group = identityService.createGroupQuery().groupId(organName).singleResult();
            allowed = group.getType().equalsIgnoreCase(role.nID_Role);
        }
        else {
            List<Group> groups = identityService.createGroupQuery().list();
            for (Group group : groups) {
                if (group.getType().equalsIgnoreCase(role.nID_Role)) {
                    allowed = true;
                    break;
                }
            }
        }
        LOG.info("Action allowed: " + allowed);
        return allowed;
    }
}
