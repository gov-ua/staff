package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Governance extends NamedEntity implements WfClonable<Governance>{
    @Transient
    @Override
    public Governance clone() throws CloneNotSupportedException {
        return (Governance)super.clone();
    }
}
