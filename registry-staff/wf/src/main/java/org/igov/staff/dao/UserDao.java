package org.igov.staff.dao;

import org.igov.staff.model.User;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface UserDao extends EntityDao<Long, User> {  
    
    public User getUserByLogin(String sLogin);
}
