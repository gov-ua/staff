package org.igov.staff.dao;

import org.igov.staff.model.Categori;
import org.igov.model.core.EntityDao;

public interface CategoryDao extends EntityDao<Long, Categori> {

}

