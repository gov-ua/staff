package org.igov.staff.model;

import org.hibernate.annotations.Type;
import org.igov.util.JSON.JsonDateDeserializer;
import org.igov.util.JSON.JsonDateSerializer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.Column;
import javax.persistence.Transient;
import org.igov.model.core.AbstractEntity;
import org.joda.time.DateTime;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Period extends AbstractEntity implements WfClonable<Period>{
    
    @JsonProperty(value = "nYear")
    @Column
    private Integer nYear      ;
    
    @JsonProperty(value = "nQuater")
    @Column
    private Integer nQuater    ;
    
    @JsonProperty(value = "sDateStart")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private DateTime    oDateStart ;
    
    @JsonProperty(value = "sDateFinish")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private DateTime    oDateFinish;

    public Period() { }

    public Period(Integer nYear, Integer nQuater, DateTime oDateStart, DateTime oDateFinish) {
        this.nYear = nYear;
        this.nQuater = nQuater;
        this.oDateStart = oDateStart;
        this.oDateFinish = oDateFinish;
    }

    public Integer getnYear() {
        return nYear;
    }

    public void setnYear(Integer nYear) {
        this.nYear = nYear;
    }

    public Integer getnQuater() {
        return nQuater;
    }

    public void setnQuater(Integer nQuater) {
        this.nQuater = nQuater;
    }

    public DateTime getoDateStart() {
        return oDateStart;
    }

    public void setoDateStart(DateTime oDateStart) {
        this.oDateStart = oDateStart;
    }

    public DateTime getoDateFinish() {
        return oDateFinish;
    }

    public void setoDateFinish(DateTime oDateFinish) {
        this.oDateFinish = oDateFinish;
    }
    
    @Transient
    @Override
    public Period clone() throws CloneNotSupportedException {
        return (Period)super.clone();
    }

}
