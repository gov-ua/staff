package org.igov.staff.dao;

import org.igov.staff.model.SubjectReport_StructureOrgan;
import org.igov.model.core.EntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
public interface SubjectReport_StructureOrganDao extends EntityDao<Long, SubjectReport_StructureOrgan> {

}
