package org.igov.staff.rest.controller;

import io.swagger.annotations.*;
import org.activiti.engine.ProcessEngines;
import org.igov.model.action.task.core.entity.LoginResponse;
import org.igov.model.action.task.core.entity.LoginResponseI;
import org.igov.service.exception.AccessServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * User: goodg_000
 * Date: 06.10.2015
 * Time: 22:57
 */
@Controller
@RequestMapping(value = "/accessSubject")
public class AccessController {

    private static final Logger LOG = LoggerFactory.getLogger(AccessController.class);

    /**
     * Логин пользователя в систему. Возращает признак успеха/неудачи входа.
     * true - Пользователь авторизирован
     * false - Имя пользователя или пароль некорректны
     *
     * @param login    - Логин пользователя
     * @param password - Пароль пользователя
     * @return {"session":"true"} -- Пользователь авторизирован
     * OR  {"session":"false"}- Имя пользователя или пароль некорректны
     * @throws AccessServiceException
     */
    @RequestMapping(value = { "/login", "/login-v2" }, method = RequestMethod.POST)
    //@RequestMapping(value = { "/loginSubject" }, method = RequestMethod.POST)
    public
    @ResponseBody
    LoginResponseI login(
            @ApiParam(value = "Строка логин пользователя", required = true) @RequestParam(value = "sLogin") String login,
            @ApiParam(value = "Строка пароль пользователя", required = true) @RequestParam(value = "sPassword") String password,
            HttpServletRequest request)
            throws AccessServiceException {
        if (ProcessEngines.getDefaultProcessEngine().getIdentityService().checkPassword(login, password)) {
            request.getSession(true);
            return new LoginResponse(Boolean.TRUE.toString());
        } else {
            throw new AccessServiceException(AccessServiceException.Error.LOGIN_ERROR, "Login or password invalid");
        }
    }
}
