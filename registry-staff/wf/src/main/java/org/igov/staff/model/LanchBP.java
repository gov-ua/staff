package org.igov.staff.model;

/**
 * Created by Sergey_PC on 24.10.2015.
 */

import org.hibernate.annotations.Type;
import org.igov.util.JSON.JsonDateDeserializer;
import org.igov.util.JSON.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.*;
import org.igov.model.core.AbstractEntity;
import org.joda.time.DateTime;

@javax.persistence.Entity
public class LanchBP extends AbstractEntity implements WfClonable<LanchBP> {

    @JsonProperty(value = "sID_BP")
    @Column
    private String sID_BP;

    @JsonProperty(value = "oStatusBP")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_StatusBP", nullable = false)
    private StatusBP oStatusBP;

    @JsonProperty(value = "sDateStart")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private DateTime oDateStart;

    @JsonProperty(value = "sDateFinish")
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
    @Type(type = DATETIME_TYPE)
    @Column
    private DateTime oDateFinish;

    @JsonProperty(value = "oSubjectReport_Structure")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_SubjectReport_StructureOrgan", nullable = false)
    private SubjectReport_StructureOrgan oSubjectReport_StructureOrgan;

    public String getsID_BP() {
        return sID_BP;
    }

    public void setsID_BP(String sID_BP) {
        this.sID_BP = sID_BP;
    }

    public StatusBP getoStatusBP() {
        return oStatusBP;
    }

    public void setoStatusBP(StatusBP oStatusBP) {
        this.oStatusBP = oStatusBP;
    }

    public DateTime getoDateStart() {
        return oDateStart;
    }

    public void setoDateStart(DateTime oDateStart) {
        this.oDateStart = oDateStart;
    }

    public DateTime getoDateFinish() {
        return oDateFinish;
    }

    public void setoDateFinish(DateTime oDateFinish) {
        this.oDateFinish = oDateFinish;
    }

    public SubjectReport_StructureOrgan getoSubjectReport_StructureOrgan() {
        return oSubjectReport_StructureOrgan;
    }

    public void setoSubjectReport_StructureOrgan(SubjectReport_StructureOrgan oSubjectReport_StructureOrgan) {
        this.oSubjectReport_StructureOrgan = oSubjectReport_StructureOrgan;
    }

    @Transient
    @Override
    public LanchBP clone() throws CloneNotSupportedException {
        return (LanchBP)super.clone();
    }

}
