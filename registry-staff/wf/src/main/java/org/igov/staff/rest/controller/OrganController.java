package org.igov.staff.rest.controller;

import com.google.common.base.Optional;
import java.util.ArrayList;
import org.igov.staff.dao.OrganDao;
import org.igov.staff.model.Organ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.igov.staff.activiti.Util;
import org.igov.staff.dao.GovernanceDao;
import org.igov.staff.dao.OrganTypeCommonDao;
import org.igov.staff.dao.OrganTypeDetailedDao;
import org.igov.staff.dao.SubmissionDao;
import org.igov.staff.model.Governance;
import org.igov.staff.model.OrganTypeCommon;
import org.igov.staff.model.OrganTypeDetailed;
import org.igov.staff.model.Submission;

/**
 * Created by Sergey_PC on 28.11.2015.
 */
@Controller
@RequestMapping(value = "/staff", produces = {MediaType.APPLICATION_JSON_VALUE})
public class OrganController {

    @Autowired
    private OrganDao organDao;
    @Autowired
    private GovernanceDao governanceDao;
    @Autowired
    private SubmissionDao submissionDao;
    @Autowired
    private OrganTypeCommonDao organTypeCommonDao;
    @Autowired
    private OrganTypeDetailedDao organTypeDetailedDao;

    @Autowired
    private IdentityService identityService;

    @RequestMapping(value = "/setOrgan", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody Organ setOrgan(@RequestParam String name,
            @RequestParam String sNameShort,
            @RequestParam Long nID_Governance,
            @RequestParam Long nID_Submission,
            @RequestParam Long nID_OrganTypeCommon,
            @RequestParam Long nID_OrganTypeDetailed) {
        Organ organ = new Organ();
        organ.setName(name);
        Governance governance = governanceDao.findByIdExpected(nID_Governance);
        organ.setoGovernance(governance);
        Submission submission = submissionDao.findByIdExpected(nID_Submission);
        organ.setoSubmission(submission);
        OrganTypeCommon organTypeCommon = organTypeCommonDao.findByIdExpected(nID_OrganTypeCommon);
        organ.setoOrganTypeCommon(organTypeCommon);
        OrganTypeDetailed organTypeDetailed = organTypeDetailedDao.findByIdExpected(nID_OrganTypeDetailed);
        organ.setoOrganTypeDetailed(organTypeDetailed);
        organ.setsAddress("");
        organ.setsEmail("");
        organ.setsIndex("");
        organ.setsPhone("");
        organDao.saveOrUpdate(organ);
        return organ;
    }
    
    @RequestMapping(value = "/updateOrgan", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody Organ updateOrgan(@RequestParam Long id,
            @RequestParam String name,
            @RequestParam String sNameShort,
            @RequestParam Long nID_Governance,
            @RequestParam Long nID_Submission,
            @RequestParam Long nID_OrganTypeCommon,
            @RequestParam Long nID_OrganTypeDetailed) {
        Organ organ = organDao.findByIdExpected(id);
        organ.setName(name);
        Governance governance = governanceDao.findByIdExpected(nID_Governance);
        organ.setoGovernance(governance);
        Submission submission = submissionDao.findByIdExpected(nID_Submission);
        organ.setoSubmission(submission);
        OrganTypeCommon organTypeCommon = organTypeCommonDao.findByIdExpected(nID_OrganTypeCommon);
        organ.setoOrganTypeCommon(organTypeCommon);
        OrganTypeDetailed organTypeDetailed = organTypeDetailedDao.findByIdExpected(nID_OrganTypeDetailed);
        organ.setoOrganTypeDetailed(organTypeDetailed);
        organ.setsAddress("");
        organ.setsEmail("");
        organ.setsIndex("");
        organ.setsPhone("");
        organ.setsFax("");
        organDao.saveOrUpdate(organ);
        return organ;
    }

    @RequestMapping(value = "/updateNameOrganByNameOld", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody Organ updateNameOrganByNameOld(@RequestParam String nameOld,
            @RequestParam String nameNew) {
        Organ organ = null;
        Optional<Organ> organOptional = organDao.findBy("name", nameOld);
        if (organOptional.isPresent()) {
            organ = organOptional.get();
            organ.setName(nameNew);
            organDao.saveOrUpdate(organ);
        }
        return organ;
    }

    @RequestMapping(value = "/updateNameOrganById", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody Organ updateNameOrganById(@RequestParam Long id,
            @RequestParam String nameNew) {
        Organ organ = organDao.findByIdExpected(id);
        organ.setName(nameNew);
        organDao.saveOrUpdate(organ);
        return organ;
    }
    
    @RequestMapping(value = "/removeOrgan", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody boolean removeOrgan(@RequestParam Long id) {
        organDao.delete(id);
        return true;
    }

    @RequestMapping(value = "/getOrgan", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Organ> getAOrgans(@RequestParam String name) {
        return organDao.findAllBy("name", name);
    }

    @RequestMapping(value = "/getOrgans", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Organ> getAOrgans() {
        return organDao.findAll();
    }

    @RequestMapping(value = "/getGroups", produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    List<Group> getGroups() {
        List<Group> groups = identityService.createGroupQuery().orderByGroupName().asc().list();
        List<Group> result = new ArrayList<Group>();
        for (Group group : groups) {
            if (!Util.isItAdmin(group.getId())) {
                result.add(group);
            }
        }
        return result;
    }

    @RequestMapping(value = "/getOrganByUser", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody
    Organ getOrganByUser(@RequestParam String userId) {
        Organ organ = null;
        List<Group> groups = identityService.createGroupQuery().groupMember(userId).orderByGroupName().asc().list();
        Group group = (groups != null && groups.size() > 0) ? groups.get(0) : null;
        if (group != null) {
            Optional<Organ> organOpt = organDao.findBy("name", group.getId());
            if (organOpt.isPresent()) {
                organ = organOpt.get();
                organ.setsRight(group.getType());
            }
        }
        return organ;
    }
}
