package org.igov.staff.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import org.igov.model.core.AbstractEntity;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@javax.persistence.Entity
@Table(name = "StructureOrgan")
public class StructureOrgan extends AbstractEntity implements WfClonable<StructureOrgan>{

    @JsonProperty(value = "oOrgan")
    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "nID_Organ", nullable = true)
    private Organ oOrgan;
    
    @JsonProperty(value = "oStructure")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Structure", nullable = false)
    private Structure oStructure;
    
    @JsonIgnore
//    @JsonProperty(value = "aSubjectReport_StructureOrgan")
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "oStructureOrgan", fetch = FetchType.LAZY)
    @LazyCollection(LazyCollectionOption.TRUE)
    private List<SubjectReport_StructureOrgan> aSubjectReport_StructureOrgan = new ArrayList<>();

    public StructureOrgan(){}

    public Organ getoOrgan() {
        return oOrgan;
    }

    public void setoOrgan(Organ oOrgan) {
        this.oOrgan = oOrgan;
    }

    public Structure getoStructure() {
        return oStructure;
    }

    public void setoStructure(Structure oStructure) {
        this.oStructure = oStructure;
    }

    public List<SubjectReport_StructureOrgan> getaSubjectReport_StructureOrgan() {
        return aSubjectReport_StructureOrgan;
    }

    public void setaSubjectReport_StructureOrgan(List<SubjectReport_StructureOrgan> aSubjectReport_StructureOrgan) {
        this.aSubjectReport_StructureOrgan = aSubjectReport_StructureOrgan;
    }
    @JsonIgnore
    @Transient
    @Override
    public StructureOrgan clone() throws CloneNotSupportedException {
        return (StructureOrgan)super.clone();
    }
    @JsonIgnore
    @Transient
    public StructureOrgan getCopyWithoutReportStructureOrgan() throws CloneNotSupportedException {
        StructureOrgan o = this.clone();
        o.setaSubjectReport_StructureOrgan(null);
        return o;
    }

}
