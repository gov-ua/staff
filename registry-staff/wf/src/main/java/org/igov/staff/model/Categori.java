package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

@javax.persistence.Entity
public class Categori extends NamedEntity implements WfClonable<Categori> {

    @Transient
    @Override
    public Categori clone() throws CloneNotSupportedException {
        return (Categori) super.clone();
    }

    @Transient
    public boolean isActual() {
        return getId() > 7;
    }
}
