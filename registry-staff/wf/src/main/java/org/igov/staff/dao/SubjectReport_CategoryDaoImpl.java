package org.igov.staff.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.igov.staff.model.SubjectReport_Category;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class SubjectReport_CategoryDaoImpl extends GenericEntityDao<Long, SubjectReport_Category> implements SubjectReport_CategoryDao {

    private static final Logger LOG = Logger.getLogger(PositionDaoImpl.class);

    protected SubjectReport_CategoryDaoImpl() {
        super(SubjectReport_Category.class);
    }

    @Override
    public List<SubjectReport_Category> getSubjectReport_CategoriesByYearAndQuater(Integer nYearFrom, Integer nYearTo, 
            Integer nQuaterFrom, Integer nQuaterTo, String sOrganName) {
        
        Map<String, Object> params = new HashMap<>();
        params.put("yearFrom", nYearFrom);
        params.put("yearTo", nYearTo);
        params.put("quaterFrom", nQuaterFrom);
        params.put("quaterTo", nQuaterTo);
        
        StringBuilder hql = new StringBuilder("from SubjectReport_Category c")
                .append(" join fetch c.oSubjectReport_Position p")
                .append(" join fetch p.oSubjectReport_StructureOrgan so")
                .append(" where so.oPeriod.nYear >= :yearFrom")
                .append(" and so.oPeriod.nYear <= :yearTo")
                .append(" and so.oPeriod.nQuater >= :quaterFrom")
                .append(" and so.oPeriod.nQuater <= :quaterTo");
        
        if (sOrganName != null) {
            hql.append(" and so.oStructureOrgan.oOrgan.name = :organName");
            params.put("organName", sOrganName);
        }
        
        hql.append(" order by so.oStructureOrgan.oOrgan.sNameShort asc,")
                .append(" so.oStructureOrgan.oStructure.name asc,")
                .append(" p.oPosition.name asc,")
                .append(" c.oCategory.name asc");
        
        
        Query query = getSession().createQuery(hql.toString());
        
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        
        return query.list();
    }
}
