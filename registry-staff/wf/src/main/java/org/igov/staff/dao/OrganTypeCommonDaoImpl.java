package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.OrganTypeCommon;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 25.10.2015.
 */
@Repository
public class OrganTypeCommonDaoImpl extends GenericEntityDao<Long, OrganTypeCommon> implements OrganTypeCommonDao {

    private static final Logger log = Logger.getLogger(OrganTypeCommonDaoImpl.class);

    protected OrganTypeCommonDaoImpl() {
        super(OrganTypeCommon.class);
    }

}
