package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.igov.staff.model.Structure;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
public class StructureDaoImpl extends GenericEntityDao<Long, Structure> implements StructureDao {

    private static final Logger log = Logger.getLogger(PositionDaoImpl.class);

    protected StructureDaoImpl() {
        super(Structure.class);
    }
}
