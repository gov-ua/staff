package org.igov.staff.activiti.task.listener;

import java.util.Date;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.task.Task;
import org.igov.staff.activiti.serviceTask.SendForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("setDataProcessing")
public class SetDataProcessing implements TaskListener{
    
    private static final Logger LOG = LoggerFactory.getLogger(SendForm.class);

    @Override
    public void notify(DelegateTask delegateTask) {
        DelegateExecution execution = delegateTask.getExecution();
        Task historicTask = execution.getEngineServices().getTaskService()
                .createTaskQuery()
                .processInstanceId(execution.getProcessInstanceId())
                .singleResult();
        execution.setVariable("userFillFIO", historicTask.getAssignee());
        execution.setVariable("dateFillFinish", new Date());
    }

}
