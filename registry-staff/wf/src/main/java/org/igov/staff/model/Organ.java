package org.igov.staff.model;

import javax.persistence.Column;
import javax.persistence.Transient;

import org.igov.model.core.NamedEntity;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@javax.persistence.Entity
public class Organ extends NamedEntity implements WfClonable<Organ> {
    
    @JsonProperty(value = "sNameShort")
    @Column
    private String sNameShort;
    
    @JsonProperty(value = "oGovernance")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Governance")
    private Governance oGovernance;
    
    @JsonProperty(value = "oSubmission")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_Submission")
    private Submission oSubmission;
    
    @JsonProperty(value = "oOrganTypeCommon")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_OrganTypeCommon")
    private OrganTypeCommon oOrganTypeCommon;
    
    @JsonProperty(value = "oOrganTypeDetailed")
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "nID_OrganTypeDetailed")
    private OrganTypeDetailed oOrganTypeDetailed;

    @JsonProperty(value = "sAddress")
    @Column
    private String sAddress;

    @JsonProperty(value = "sIndex")
    @Column
    private String sIndex;

    @JsonProperty(value = "sPhone")
    @Column
    private String sPhone;

    @JsonProperty(value = "sFax")
    @Column
    private String sFax;

    @JsonProperty(value = "sEmail")
    @Column
    private String sEmail;

    @JsonProperty(value = "sGroup")
    @Transient
    private String sGroup;
    
    @JsonProperty(value = "sRight")
    @Transient
    private String sRight;

    public Organ() {}

    public String getsAddress() {
        return sAddress;
    }

    public void setsAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public String getsPhone() {
        return sPhone;
    }

    public void setsPhone(String sPhone) {
        this.sPhone = sPhone;
    }

    public String getsFax() {
        return sFax;
    }

    public void setsFax(String sFax) {
        this.sFax = sFax;
    }

    public String getsEmail() {
        return sEmail;
    }

    public void setsEmail(String sEmail) {
        this.sEmail = sEmail;
    }

    public String getsIndex() {
        return sIndex;
    }

    public void setsIndex(String sIndex) {
        this.sIndex = sIndex;
    }

    public Governance getoGovernance() {
        return oGovernance;
    }

    public void setoGovernance(Governance oGovernance) {
        this.oGovernance = oGovernance;
    }

    public Submission getoSubmission() {
        return oSubmission;
    }

    public void setoSubmission(Submission oSubmission) {
        this.oSubmission = oSubmission;
    }

    public OrganTypeCommon getoOrganTypeCommon() {
        return oOrganTypeCommon;
    }

    public void setoOrganTypeCommon(OrganTypeCommon oOrganTypeCommon) {
        this.oOrganTypeCommon = oOrganTypeCommon;
    }

    public OrganTypeDetailed getoOrganTypeDetailed() {
        return oOrganTypeDetailed;
    }

    public void setoOrganTypeDetailed(OrganTypeDetailed oOrganTypeDetailed) {
        this.oOrganTypeDetailed = oOrganTypeDetailed;
    }

    public String getsGroup() {
        return sGroup;
    }

    public void setsGroup(String sGroup) {
        this.sGroup = sGroup;
    }

    public String getsRight() {
        return sRight;
    }

    public void setsRight(String sRight) {
        this.sRight = sRight;
    }
    
    public String getsNameShort() {
        return sNameShort;
    }

    public void setsNameShort(String sNameShort) {
        this.sNameShort = sNameShort;
    }

    @Transient
    @Override
    public Organ clone() throws CloneNotSupportedException {
        return (Organ) super.clone();
    }

}
