package org.igov.staff.model;

/**
 * Created by Sergey_PC on 04.11.2015.
 */
public interface WfClonable<T> extends Cloneable{
    public T clone() throws CloneNotSupportedException;
}
