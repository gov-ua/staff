package org.igov.staff.model;

import org.igov.model.core.NamedEntity;

import javax.persistence.Transient;

/**
 * Created by Sergey_PC on 24.10.2015.
 */
@javax.persistence.Entity
public class Position extends NamedEntity implements WfClonable<Position> {

    public Position() {
    }

    @Transient
    @Override
    public Position clone() throws CloneNotSupportedException {
        return (Position) super.clone();
    }

    @Transient
    public boolean isActual() {
        return getId() > 4;
    }

}
