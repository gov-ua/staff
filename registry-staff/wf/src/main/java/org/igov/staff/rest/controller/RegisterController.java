package org.igov.staff.rest.controller;

import com.google.common.base.Optional;
import java.util.List;
import javassist.NotFoundException;
import javax.servlet.http.HttpServletResponse;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.User;
import org.igov.staff.dao.OrganDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.igov.staff.dao.UserDao;
import org.igov.staff.model.Organ;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Sergey_PC on 28.11.2015.
 */
@Controller
@RequestMapping(value = "/staff", produces = {MediaType.APPLICATION_JSON_VALUE})
public class RegisterController {

    private static final Logger LOG = LoggerFactory.getLogger(RegisterController.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private OrganDao organDao;

    @Autowired
    private IdentityService identityService;

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    void register(@RequestBody org.igov.staff.model.User user, HttpServletResponse httpResponse) throws NotFoundException {
        if(user.getsOrgan()    == null) throw new NotFoundException("Невказанний орган"  );
        if(user.getsPosition() == null) throw new NotFoundException("Невказанна посада"  );
        if(user.getsLastName() == null) throw new NotFoundException("Невказанне прізвище");
        if(user.getsFirstName()== null) throw new NotFoundException("Невказанне Iм'я"    );
        Optional<Organ> optionalOrgan = organDao.findBy("name", user.getsOrgan());
        if(!optionalOrgan.isPresent()){
            httpResponse.setStatus(404);
            throw new NotFoundException("Орган не знайденний");
        }
        
        Organ organ = optionalOrgan.get();
        List<User> usersFromGroup = identityService.createUserQuery().memberOfGroup(optionalOrgan.get().getName()).list();
        if(usersFromGroup != null && usersFromGroup.size() > 1){
            throw new IllegalArgumentException("Орган вже має двох користувачів! Зайдіть до порталу під існуючим користувачем!");
        }
        
        User userAct = identityService.createUserQuery().userId(user.getsLogin()).singleResult();
        if(userAct!= null){
            httpResponse.setStatus(409);
            throw new IllegalArgumentException("Логін вже використовується");
        }

        user.setoOrgan(organ);

        User userActiviti = identityService.newUser(user.getsLogin());

        userActiviti.setFirstName(user.getsFirstName() + " " + user.getsMiddleName());
        userActiviti.setLastName(user.getsLastName());
        userActiviti.setPassword(user.getsPassword());
        userActiviti.setEmail(user.getsEmail());
        identityService.saveUser(userActiviti);

        LOG.info("create activiti user ok!");
        identityService.createMembership(userActiviti.getId(), user.getoOrgan().getName());
        LOG.info("create activiti membership ok!");
        userDao.saveOrUpdate(user);
        LOG.info("create user ok!");
    }
}
