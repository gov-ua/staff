package org.igov.staff.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.igov.staff.model.Organ;
import org.springframework.stereotype.Repository;
import org.igov.model.core.GenericEntityDao;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
@Repository
public class OrganDaoImpl extends GenericEntityDao<Long, Organ> implements OrganDao  {

    private static final Logger log = Logger.getLogger(OrganDaoImpl.class);

    protected OrganDaoImpl() {
        super(Organ.class);
    }
    
    @Override
    public Organ getOrgan(String sName) {
        Criteria criteria = getSession().createCriteria(Organ.class);
        criteria.add(Restrictions.eq("name", sName));
        Organ organ = (Organ) criteria.uniqueResult();
        return organ;
    }
}
