package org.igov.staff.dao;

import org.igov.staff.model.StructureOrgan;
import org.igov.model.core.EntityDao;

import java.util.List;

/**
 * Created by Sergey_PC on 26.10.2015.
 */
public interface StructureOrganDao extends EntityDao<Long, StructureOrgan> {

   public List<StructureOrgan> getaStructureOrganByID_Organ(Long in);
}
