package org.igov.staff.dao;

import org.igov.staff.model.Governance;
import org.igov.model.core.EntityDao;

public interface GovernanceDao extends EntityDao<Long, Governance> {

}

