package org.igov.staff.activiti.serviceTask;

/**
 * Created by Sergey_PC on 22.11.2015.
 */
public class FormForSave {
    private String field  = null;
    private Long position = null;
    private Long category = null;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }
}
